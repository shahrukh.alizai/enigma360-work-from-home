----------------------------------------------------------------
#List comprehension
#Print the list in lexicographic increasing order.

if __name__ == '__main__':
    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input())
arr = [[i, j, k] for i in range(x+1) for j in range(y+1) for k in range(z+1) if i + j + k != n]
print(arr)

----------------------------------------------------------------
#runnerup score

if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())

print(sorted(list(set(arr)))[-2])

----------------------------------------------------------------
#nested lists

marksheet=[]
scoresheet=[]
if __name__ == '__main__':
    for _ in range(int(input())):
        name = input()
        score = float(input())
        marksheet+=[[name,score]]
        scoresheet+=[score]
    x=sorted(set(scoresheet))[1]
    for n,s in sorted(marksheet):
        if s==x:
            print(n) 

---------------------------------------------------------------

#finding the percentage

    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
print("{0:.2f}".format(*[(sum(scores)/len(scores))for name,scores in student_marks.items() if name==query_name]))

---------------------------------------------------------------
