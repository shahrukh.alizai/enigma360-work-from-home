-----------------------------------------------------------

#CheckStrictSuperSubset
A=set(input().split())
n=int(input())
issupersubset=True
for i in range(n):
    b=set(input().split())
    if not A.issuperset(b):
        issupersubset=False
    if len(b)>len(A):
        issupersubset=False
print(issupersubset)

-----------------------------------------------------------
