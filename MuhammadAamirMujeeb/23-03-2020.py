#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the birthday function below.
def birthday(s, d, m):
    ans=0
    for i in range(len(s)):
        n=0
        count=0
        while(n<(m)):
            count+=s[i+n]
            n+=1
        if(count==d):
            ans+=1
        if(i+n==len(s)):
            break
    return ans
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    s = list(map(int, input().rstrip().split()))

    dm = input().rstrip().split()

    d = int(dm[0])

    m = int(dm[1])

    result = birthday(s, d, m)

    fptr.write(str(result) + '\n')

    fptr.close()
--------------------------------------------------------
#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the bonAppetit function below.
def bonAppetit(bill, k, b):
    count=0
    for i in range(len(bill)):
        if (i!=k):
            count+=bill[i]
    pay=count//2
    if (pay!=b):
        print(b-pay)
    else:
        print("Bon Appetit")
if __name__ == '__main__':
    nk = input().rstrip().split()

    n = int(nk[0])

    k = int(nk[1])

    bill = list(map(int, input().rstrip().split()))

    b = int(input().strip())

    bonAppetit(bill, k, b)
----------------------------------------------
#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the breakingRecords function below.
def breakingRecords(scores):
    maxi=scores[0]
    mini=scores[0]
    maxcount=0
    mincount=0
    for i in range(len(scores)):
        if(scores[i]>maxi):
            maxi=scores[i]
            maxcount+=1
        if(scores[i]<mini):
            mini=scores[i]
            mincount+=1
    return(maxcount,mincount)
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    scores = list(map(int, input().rstrip().split()))

    result = breakingRecords(scores)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
------------------------------------------------------
#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'getTotalX' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b
#

def getTotalX(a, b):
    lcm_num = a[0]
    gcd_num = b[0]
    if len(a) > 1:
        for x in range(1,len(a)):
            lcm_num =  (lcm_num * a[x])//math.gcd(lcm_num,a[x])
    if len(b) > 1:
        for x in range(1,len(b)):
            gcd_num = math.gcd(gcd_num,b[x])
    count = 0
    for x in range(lcm_num, gcd_num+1, lcm_num):
        if math.gcd(x, gcd_num) == x:
            count += 1
    return count

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    arr = list(map(int, input().rstrip().split()))

    brr = list(map(int, input().rstrip().split()))

    total = getTotalX(arr, brr)

    fptr.write(str(total) + '\n')

    fptr.close()
----------------------------------------------------------
#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the catAndMouse function below.
def catAndMouse(x, y, z):
    if abs(x-z)<abs(y-z):
        return "Cat A"
    elif abs(x-z)>abs(y-z):
        return "Cat B"
    elif abs(x-z)==abs(y-z):
        return "Mouse C"
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        xyz = input().split()

        x = int(xyz[0])

        y = int(xyz[1])

        z = int(xyz[2])

        result = catAndMouse(x, y, z)

        fptr.write(result + '\n')

    fptr.close()
